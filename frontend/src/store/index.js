import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import createPersistedState from "vuex-persistedstate";
import auth from './modules/auth';
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
    },
    mutations: {
      
    },
    actions: {
      
    },
  
    modules: {
    },
    getters: {
    }
})
  