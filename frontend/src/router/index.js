import Vue from 'vue'
import VueRouter from 'vue-router'
import FrontPage from '../views/FrontPage.vue'

Vue.use(VueRouter) 

const routes = [
  {
    path: '/',
    name: 'FrontPage',
    component: FrontPage
  },
  {
    path: '/dnevni_obroci',
    name: 'DailyMeals',
    component: () => import('../views/DailyMeals.vue')
  },
  {
    path: '/recepti',
    name: 'Recipes',
    component: () => import('../views/Recipes.vue')
  },
  {
    path: '/obroci',
    name: 'Meals',
    component: () => import('../views/Meals.vue')
  },
  // {
  //   path: '/:categoryId/:articleId',
  //   name: 'Article',
  //   component: () => import('../views/Article.vue')
  // },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
