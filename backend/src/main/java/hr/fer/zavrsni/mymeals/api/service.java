package hr.fer.zavrsni.mymeals.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import hr.fer.zavrsni.mymeals.model.*;

@Service
public class service {
	
	public ResponseEntity<Map<JSONObject, byte[]>> produceRecipes(Set<Recipe> recipeSet) throws JSONException {
        Map<JSONObject, byte[]> recipeMap = new HashMap<>();
        for (Recipe c : recipeSet) {
        	recipeMap.put(produceRecipeJson(c), c.getImageInBytes());
        }
        System.out.println("Velicina mape je : " + recipeMap.size());
        return new ResponseEntity<>(recipeMap, HttpStatus.OK);
    }

	private JSONObject produceRecipeJson(Recipe a) throws JSONException {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": \"").append(a.getId()).append("\",");
        sb.append("\"name\": \"").append(a.getName()).append("\",");
        sb.append("\"type\": \"").append(a.getType().toString()).append("\",");
        sb.append("\"ingredients\": \"").append(a.getIngredients()).append("\",");
        sb.append("\"description\": \"").append(a.getDescription()).append("\",");
        sb.append("}");
        System.out.println(sb.toString());
        return new JSONObject(sb.toString());
    }
	
	public ResponseEntity<Map<UUID, JSONObject>> produceDailyMeals(Set<DailyMeals> dailyMealsSet) throws JSONException {
        Map<UUID, JSONObject> dailyMealsMap = new HashMap<>();
        for (DailyMeals c : dailyMealsSet) {
        	dailyMealsMap.put(c.getId(), produceDailyMealsJson(c));
        }
        System.out.println("Velicina mape je : " + dailyMealsMap.size());
        return new ResponseEntity<>(dailyMealsMap, HttpStatus.OK);
    }

	private JSONObject produceDailyMealsJson(DailyMeals a) throws JSONException {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": \"").append(a.getId()).append("\",");
        sb.append("\"day\": \"").append(a.getDay()).append("\",");
        sb.append("\"meal\": \"").append(a.getMeal().toString()).append("\",");
        sb.append("\"recipe\": \"").append(a.getRecipe().toString()).append("\",");
        sb.append("}");
        System.out.println(sb.toString());
        return new JSONObject(sb.toString());
    }
}
