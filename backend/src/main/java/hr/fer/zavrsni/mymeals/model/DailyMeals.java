package hr.fer.zavrsni.mymeals.model;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;

@Entity
@Table(name="dailyMeals")
public class DailyMeals {

	@Id
    @Column(name ="id", nullable = true)
    private final UUID id;

    @Column(name="day", nullable = false)
    private Day day;
    
    @Column(name="meal", nullable = false)
    private Meal meal;
    
    @OneToOne(mappedBy = "dailyMeals", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Recipe> recipe;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    
    public DailyMeals() {
    	this.id = UUID.randomUUID();
    }
    
    public DailyMeals(Day day, Meal meal, User user) {
        this.id = UUID.randomUUID();
        this.day = day;
        this.meal = meal;
        this.recipe = new HashSet<>();
        this.user = user;
    }
    
    public UUID getId() {
    	return id;
    }
    
    public Day getDay() {
    	return day;
    }
    
    public void setDay(Day day) {
    	this.day = day;
    }
    
    public Meal getMeal() {
    	return meal;
    }
    
    public void setMeal(Meal meal) {
    	this.meal = meal;
    }
    
    public Set<Recipe> getRecipe() {
    	return recipe;
    }
    
    public void setRecipe(Set<Recipe> recipe) {
    	this.recipe = recipe;
    }
}

