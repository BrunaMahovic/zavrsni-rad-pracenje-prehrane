package hr.fer.zavrsni.mymeals.dao;

import java.util.Set;
import java.util.UUID;

import com.sun.istack.NotNull;

import hr.fer.zavrsni.mymeals.model.Day;
import hr.fer.zavrsni.mymeals.model.Meal;
import hr.fer.zavrsni.mymeals.model.Recipe;

public class DailyMealsDTO {
	
    private UUID dailyMealsId;

	@NotNull
    private Day day;

	@NotNull
    private Meal meal;
	
	@NotNull
    private Set<Recipe> recipe;
    
    public UUID getDailyMealsId() {
        return dailyMealsId;
    }

    public void setDailyMealsId(UUID dailyMealsId) {
        this.dailyMealsId = dailyMealsId;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }
    
    public Meal getMeal() {
    	return meal;
    }
    
    public void setMeal(Meal meal) {
    	this.meal = meal;
    }
    
    public Set<Recipe> getRecipe() {
    	return recipe;
    }
    
    public void setRecipe(Set<Recipe> recipe) {
    	this.recipe = recipe;
    }
}
