package hr.fer.zavrsni.mymeals.security.jwt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;

import hr.fer.zavrsni.mymeals.model.User;
import hr.fer.zavrsni.mymeals.repository.Roles;
import hr.fer.zavrsni.mymeals.service.UserDetailsServiceImpl;
import static hr.fer.zavrsni.mymeals.security.WebSecurityConfiguration.loggedInUsers;
import static hr.fer.zavrsni.mymeals.security.SecurityConstants.*;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;
    private boolean loggedIn;
    private UserDetailsServiceImpl userDetailsService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserDetailsServiceImpl userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            User creds = new ObjectMapper().readValue(req.getInputStream(), User.class);
            if(loggedInUsers.containsValue(creds.getEmail())){
                loggedIn = true;
                return authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                "failed",
                                "failed",
                                new ArrayList<>()));
            }
            loggedIn = false;

            UserDetails userDetails = userDetailsService.loadUserByUsername(creds.getEmail());

            Authentication auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword(),
                            userDetails.getAuthorities()));

            if(!userDetails.getAuthorities().iterator().next().getAuthority().equals(Roles.USER.toString().toLowerCase()))
                auth.setAuthenticated(false);

            return auth;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) {
    	Date currentDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
       String token = JWT.create()
               .withSubject(((org.springframework.security.core.userdetails.User) authResult.getPrincipal()).getUsername())
               .withClaim("role", ((List<SimpleGrantedAuthority>) authResult.getAuthorities()).get(0).getAuthority())
               .withClaim("user", authResult.isAuthenticated())
               .withExpiresAt(currentDate)
               .sign(Algorithm.HMAC512(SECRET.getBytes()));
       loggedInUsers.put(token, ((org.springframework.security.core.userdetails.User) authResult.getPrincipal()).getUsername());
       response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    }


    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        SecurityContextHolder.clearContext();

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        if(loggedIn) {
            response.getWriter().write("This user is already logged in");
            System.out.println("A user that is already logged in has tried to log in");
        }
    }
}
