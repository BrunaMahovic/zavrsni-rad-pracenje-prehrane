package hr.fer.zavrsni.mymeals.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.fer.zavrsni.mymeals.model.DailyMeals;

public interface DailyMealsRepository extends JpaRepository<DailyMeals, UUID> {

	Optional<DailyMeals> findById(UUID id);
}
