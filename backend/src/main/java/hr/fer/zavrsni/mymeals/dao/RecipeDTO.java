package hr.fer.zavrsni.mymeals.dao;

import com.sun.istack.NotNull;

public class RecipeDTO {

	@NotNull
    private String name;

	@NotNull
    private String type;
	
	@NotNull
    private String ingredients;

    @NotNull
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
