package hr.fer.zavrsni.mymeals.api;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import hr.fer.zavrsni.mymeals.dao.RecipeDTO;
import hr.fer.zavrsni.mymeals.dao.UserDTO;
import hr.fer.zavrsni.mymeals.model.*;
import hr.fer.zavrsni.mymeals.repository.*;
import static hr.fer.zavrsni.mymeals.security.WebSecurityConfiguration.loggedInUsers;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
    private UserRepository userRepository;
	private RecipeRepository recipeRepository;
	@Autowired
    private service service;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public UserController(UserRepository userRepository, RecipeRepository recipeRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.recipeRepository = recipeRepository;
		this.service = new service();
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	@PostMapping(value="/registration")
    public ResponseEntity<?> registerNewUser(@RequestBody UserDTO userDTO) throws IOException {

		userDTO.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));

        if (!userRepository.existsByEmail(userDTO.getEmail())) {
            User user = new User(userDTO.getName(), userDTO.getSurname(), userDTO.getEmail(), userDTO.getPassword());
            user.setRole(Roles.USER.toString().toLowerCase());
            userRepository.save(user);
        }
        else {
            return new ResponseEntity<String>("This E-mail is taken", HttpStatus.FORBIDDEN);
        }

        return ResponseEntity.ok().body("Successful registration");
    }
	
	@PostMapping("/addRecipe")
    public ResponseEntity<?> addRecipe(@RequestPart("json") RecipeDTO recipeDTO, @RequestPart("file") MultipartFile file){

        String currentUsername = loggedInUsers.get(BearerTokenUtil.getBearerTokenHeader());

        User user = userRepository.findByEmail(currentUsername);
        if(user == null)
            return new ResponseEntity<String>("User was not found in the repository", HttpStatus.NOT_FOUND);

        List<Recipe> recipeList = recipeRepository.findByUser(user);
        for(Recipe c : recipeList)
            if(c.getName().equals(recipeDTO.getName()))
                return new ResponseEntity<String>("User already has a recipe of the same name", HttpStatus.NOT_ACCEPTABLE);
        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
        Type type;
        try{
        	type = Type.valueOf(recipeDTO.getType());
        	Recipe recipe = new Recipe(recipeDTO.getName(), type, recipeDTO.getDescription(), recipeDTO.getIngredients(), file.getBytes(), user);

            recipeRepository.save(recipe);
        } catch (IllegalArgumentException | IOException e) {
            return new ResponseEntity<String>("Style does not exist", HttpStatus.NOT_ACCEPTABLE);
        }
        
        return ResponseEntity.ok().body("Successful collection creation");
    }
	
	@PostMapping("/logout")
    public ResponseEntity<?> logout(){

        loggedInUsers.remove(BearerTokenUtil.getBearerTokenHeader());
        return ResponseEntity.ok().body("Logout successful");
    }
	
	@GetMapping(value="/getRecipes")
    @ResponseBody
    public ResponseEntity<?> getRecipes() throws JSONException {
        User user = userRepository.findByEmail(loggedInUsers.get(BearerTokenUtil.getBearerTokenHeader()));

        if(user == null)
            return new ResponseEntity<String>("User was not found in the repository", HttpStatus.NOT_FOUND);

        Set<Recipe> recipeSet = user.getRecipes();
                //collectionRepository.findByArtist(artist);

        return service.produceRecipes(recipeSet);
    }
}
