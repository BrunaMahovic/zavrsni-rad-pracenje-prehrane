package hr.fer.zavrsni.mymeals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class MyMealsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyMealsApplication.class, args);
	}

}
