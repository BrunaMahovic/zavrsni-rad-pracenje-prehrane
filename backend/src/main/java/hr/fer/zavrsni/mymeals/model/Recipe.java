package hr.fer.zavrsni.mymeals.model;

import java.util.UUID;

import javax.persistence.*;

@Entity
@Table(name="recipe")
public class Recipe {

	@Id
    @Column(name ="id", nullable = false)
    private final UUID id;

    @Column(name="name", nullable = false)
    private String name;
    
    @Column(name="type", nullable = false)
    private Type type;

    @Column(name="ingredients", nullable = false)
    private String ingredients;

    @Column(name="description", nullable = false)
    private String description;

    @Column(name="image", nullable = false)
    private byte[] imageInBytes;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    
    @OneToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "daily_meals_id", nullable = true)
    private DailyMeals dailyMeals;
    
    public Recipe(){
        this.id = UUID.randomUUID();
    }

    public Recipe(String name, Type type, String ingredients, String description, byte[] imageInBytes, User user) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.type = type;
        this.ingredients = ingredients;
        this.description = description;
        this.imageInBytes = imageInBytes;
        this.user = user;
    }
    
    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Type getType() {
        return type;
    }
    
    public void setType(Type type) {
        this.type = type;
    }
    
    public String getIngredients() {
        return ingredients;
    }
    
    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public byte[] getImageInBytes() {
        return imageInBytes;
    }

    public void setImageInBytes(byte[] imageInBytes) {
        this.imageInBytes = imageInBytes;
    }
    
    public User getUser() {
    	return user;
    }
    
    public void setUser(User user) {
    	this.user = user;
    }
    
    public DailyMeals getDailyMeals() {
    	return dailyMeals;
    }
    
    public void setDailyMeals(DailyMeals dailyMeals) {
    	this.dailyMeals = dailyMeals;
    }
}
