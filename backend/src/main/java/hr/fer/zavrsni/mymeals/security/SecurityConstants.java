package hr.fer.zavrsni.mymeals.security;

public class SecurityConstants {

	public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/user/registration";
    public static final String ADD_RECIPE = "/user/addRecipe";
    public static final String GET_RECIPE = "/user/getRecipe";
    public static final String ADD_DAILYMEALS = "/dailyMeals/addDailyMeals";
    public static final String GET_DAILYMEALS = "/dailyMeals/getDailyMeals";
    public static final String LOGOUT = "/user/logout";
}
