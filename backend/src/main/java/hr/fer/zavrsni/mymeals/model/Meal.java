package hr.fer.zavrsni.mymeals.model;

public enum Meal {
	
	BREAKFAST, LUNCH, DINNER
}
