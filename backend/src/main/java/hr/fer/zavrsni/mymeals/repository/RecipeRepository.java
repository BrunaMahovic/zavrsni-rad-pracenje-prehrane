package hr.fer.zavrsni.mymeals.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.mymeals.model.Recipe;
import hr.fer.zavrsni.mymeals.model.User;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, UUID> {

	List<Recipe> findByUser(User user);
}
