package hr.fer.zavrsni.mymeals.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.zavrsni.mymeals.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, UUID>{

	User findByEmail(String email);
    boolean existsByEmail(String email);
}
