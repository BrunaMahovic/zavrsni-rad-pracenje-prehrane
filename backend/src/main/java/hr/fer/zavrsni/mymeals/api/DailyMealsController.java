package hr.fer.zavrsni.mymeals.api;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import hr.fer.zavrsni.mymeals.dao.DailyMealsDTO;
import hr.fer.zavrsni.mymeals.model.*;
import hr.fer.zavrsni.mymeals.repository.*;
import static hr.fer.zavrsni.mymeals.security.WebSecurityConfiguration.loggedInUsers;

import java.util.Set;
import java.util.UUID;

@Controller
@RequestMapping("/dailyMeals")
public class DailyMealsController {

	private DailyMealsRepository dailyMealsRepository;
	private UserRepository userRepository;
	private RecipeRepository recipeRepository;
	@Autowired
    private service service;
	
	public DailyMealsController(DailyMealsRepository dailyMealsRepository, UserRepository userRepository, RecipeRepository recipeRepository) {
		this.dailyMealsRepository = dailyMealsRepository;
		this.userRepository = userRepository;
		this.recipeRepository = recipeRepository;
		this.service = new service();
	}
	
	@PostMapping("/addDailyMeals")
    public ResponseEntity<?> addDailyMeals(@RequestBody DailyMealsDTO dailyMealsDTO) {
        String currentEmail = loggedInUsers.get(BearerTokenUtil.getBearerTokenHeader());
        User userAdding = null;
        if (userRepository.existsByEmail(currentEmail)) {
        	userAdding = userRepository.findByEmail(currentEmail);
        } else {
            return new ResponseEntity<>("Korisnik ne postoji", HttpStatus.BAD_REQUEST);
        }
        Set<Recipe> recipes = userAdding.getRecipes();
        DailyMeals dailyMeals = new DailyMeals(dailyMealsDTO.getDay(), dailyMealsDTO.getMeal(), userAdding);
        
        for (Recipe recipe: recipes) {
        	for (Recipe recipe2: dailyMealsDTO.getRecipe()) {
        		if (recipe.equals(recipe2)) {
            		recipe.setDailyMeals(dailyMeals);
            		break;
            	}
        	}
        }
        
        dailyMealsRepository.save(dailyMeals);
        
        return new ResponseEntity<>("Obrok uspjesno dodan!", HttpStatus.OK);
    }
	
	@DeleteMapping("/removeDailyMeals")
    public ResponseEntity<?> removeDailyMeals(@RequestBody DailyMealsDTO dailyMealsDTO) {
        UUID dailyMealsId = dailyMealsDTO.getDailyMealsId();
        if (dailyMealsRepository.existsById(dailyMealsId)) {
            dailyMealsRepository.deleteById(dailyMealsId);
            return ResponseEntity.ok().body("Meal successfully deleted.");
        } else {
            return new ResponseEntity<>("Obrok s tim ID-om ne postoji", HttpStatus.BAD_REQUEST);
        }
    }
	
	@GetMapping("/getDailyMeals")
    @ResponseBody
    public ResponseEntity<?> getRecipeForDailyMeals(@RequestBody DailyMealsDTO dailyMealsDTO) throws JSONException {
       UUID dailyMealsId = dailyMealsDTO.getDailyMealsId();
       if (recipeRepository.existsById(dailyMealsId)) {
           System.out.println("\n\n\nPrimljen zahtjev za dobiti recept.");
           //return new ResponseEntity<>(new ArrayList<>(artworkRepository.findById(artworkId).get().getComments()), HttpStatus.OK);
           Set<DailyMeals> dailyMealsSet = userRepository.findById(dailyMealsId).get().getDailyMeals();
           return service.produceDailyMeals(dailyMealsSet);
       } else {
           return new ResponseEntity<>("Obrok s tim ID-om ne postoji", HttpStatus.BAD_REQUEST);
       }
    }
}
